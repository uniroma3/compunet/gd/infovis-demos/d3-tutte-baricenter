# D3 Tutte Baricenter

A D3.js project that allows you to experiment Tutte's baricenter drawing method.

## Online demo

An online demo can be found here: https://uniroma3.gitlab.io/compunet/gd/infovis-demos/d3-tutte-baricenter

## Todos

- Add some more predefined graphs server side
- Modify the reading procedure in such a way that a label is automatically added if not present in the file
- Describe the file format 
- Describe the protocol format of the generator server

## Credits

Thanks to Simone Ceccarelli and Damiano Massarelli

