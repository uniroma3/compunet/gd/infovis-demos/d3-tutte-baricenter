#!/bin/bash

if (($# < 2)) 
then 
        echo "Usage: testami.sh <numero_nodi> <numero_archi>" 
        exit 1
fi

export QUERY_STRING="nodes=$1&edges=$2"
./graph

