//ogdf
#include <ogdf/basic/Graph.h>
#include <ogdf/basic/GraphAttributes.h>
#include <ogdf/fileformats/GraphIO.h>
#include <ogdf/basic/graph_generators.h>

//combinatorial embedding
#include <ogdf/basic/CombinatorialEmbedding.h>
#include <ogdf/planarity/BoothLueker.h>


#include <sstream>

//using namespace ogdf;
using namespace std;
//using namespace httplib;

void createGML(ogdf::Graph G, std::stringstream& name){
	ogdf::GraphAttributes GA(G, ogdf::GraphAttributes::nodeLabel |
                                    ogdf::GraphAttributes::edgeLabel); // Create graph attributes for this graph

        
        //ogdf::node v;
	for(ogdf::node v : G.nodes) {
	//forall_nodes(v, G){ // iterate through all the nodes of the graph
		//ogdf::edge curre;
		string s;
		//forall_adj_edges(curre, v){
                for(ogdf::adjEntry adjG : v->adjEntries) {
			ogdf::edge curre = adjG->theEdge();

			s += to_string(curre->index()) + ",";
			GA.label(curre) = to_string(curre->index()).c_str();
		        //GA.m_edgeLabel[curre] = to_string(curre->index()).c_str();
		}
		char const *pchar = s.c_str(); //use char const* as target type
		GA.label(v) = pchar;
	        //GA.m_nodeLabel[v] = pchar;
	}
	ogdf::GraphIO::writeGML(GA, name);
}


int main(){

       int nodes = 0;
       int edges = 0;

       /* mando fuori l'header http */
  
       printf("Access-Control-Allow-Origin: *\n");
       printf("Content-TYPE: text/plain\n\n");

       string parametri = getenv("QUERY_STRING");
       string coppia;
       stringstream par_stringstream(parametri);

       while(getline(par_stringstream,coppia,'&')) {

          string nodes_string("nodes");
          string edges_string("edges");

         if (coppia.find(nodes_string) != string::npos) { // trovato nodes=...
             string nodi_stringa = coppia.substr(coppia.find_first_of("=")+1);
             nodes = atoi(nodi_stringa.c_str());
         } else if (coppia.find(edges_string) != string::npos) {  // trovato edges=...
             string archi_stringa = coppia.substr(coppia.find_first_of("=")+1);
             edges = atoi(archi_stringa.c_str());
         }

       }

       //printf("\nnodi = %d\n",nodes);
       //printf("\narchi = %d\n",edges);

       //exit(0);


       ogdf::Graph G;
       ogdf::randomPlanarTriconnectedGraph(G, nodes, edges);
       //ogdf::planarTriconnectedGraph(G, nodes, edges);
       std::stringstream output;
       createGML(G, output);
       cout << output.str();

}

/* Viene generato (con nodes=3 e edges=3) il seguente output (con il quale il client non funziona):
 
Creator "ogdf::GraphIO::writeGML"
graph
[
	directed	1
	node
	[
		id	0
		label	"2,1,0,"
	]
	node
	[
		id	1
		label	"4,0,3,"
	]
	node
	[
		id	2
		label	"3,1,5,"
	]
	node
	[
		id	3
		label	"5,2,4,"
	]
	edge
	[
		source	0
		target	1
		label "0"
	]
	edge
	[
		source	0
		target	2
		label "1"
	]
	edge
	[
		source	0
		target	3
		label "2"
	]
	edge
	[
		source	1
		target	2
		label "3"
	]
	edge
	[
		source	1
		target	3
		label "4"
	]
	edge
	[
		source	2
		target	3
		label "5"
	]
]

*/